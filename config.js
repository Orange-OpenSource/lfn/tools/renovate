module.exports = {
  "platform": "gitlab",
  "dependencyDashboardAutoclose": true,
  "autodiscover": false,
  "extends": [
    "github>whitesource/merge-confidence:beta"
  ],
  "binarySource": "install",
  "ignorePrAuthor": true,
  "onboardingConfig": {
    "extends": [
      "config:base",
      ":semanticCommitTypeAll(fix)",
      ":gitSignOff"
    ],
    "commitBodyTable": true
  },
  repositories: [
    "Orange-OpenSource/lfn/ci_cd/chained-ci-client-launcher",
    "Orange-OpenSource/lfn/ci_cd/chained-ci-project-gating",
    "Orange-OpenSource/lfn/ci_cd/chained-ci-py",
    "Orange-OpenSource/lfn/ci_cd/docker-ansible-core",
    "Orange-OpenSource/lfn/infra/infra_collection",
    "Orange-OpenSource/lfn/infra/kubernetes_collection",
    "Orange-OpenSource/lfn/infra/os_infra_manager_docker",
    "Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection",
    "Orange-OpenSource/lfn/opnfv/xtesting-api",
    "Orange-OpenSource/lfn/tools/renovate"
  ],
  hostRules: [
    {
      matchHost: 'docker.io',
      username: 'orangeoln',
      password: process.env.DOCKER_HUB_PASSWORD,
    }
  ],
};

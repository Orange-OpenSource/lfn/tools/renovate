# Renovate for Orange LFN

This project takes care of maintaining NIF CD projects dependencies up-to-date
with [Renovate](https://docs.renovatebot.com/).

It is configured  as a scheduled CI/CD job to regularly scan through some
Orange LFN projects dependencies and automatically detect new released versions
and propose upgrading those with simple Merge Requests.
